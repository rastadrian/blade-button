package com.rastadrian.androidthings

import android.app.Activity
import android.os.Bundle
import android.widget.ImageView
import com.google.android.things.pio.Gpio
import com.google.android.things.pio.GpioCallback
import com.google.android.things.pio.PeripheralManagerService

/**
 * Home Activity for the burning blade button app.
 */
class HomeActivity : Activity() {
    val mLedPin = "GPIO_34"
    val mButtonPin = "GPIO_174"

    var mImage: ImageView? = null
    var mButton: Gpio? = null
    var mLed: Gpio? = null
    val mButtonCallback = object : GpioCallback() {
        override fun onGpioEdge(button: Gpio?): Boolean {
            val buttonClicked = button?.value ?: false
            val drawable = if (buttonClicked) R.drawable.img_john_blade else R.drawable.img_john_casual

            mLed?.value = buttonClicked
            mImage?.setImageDrawable(resources.getDrawable(drawable, theme))
            return true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        mImage = findViewById(R.id.home_image) as ImageView?

        val service = PeripheralManagerService()

        mButton = service.openGpio(mButtonPin)
        mButton?.registerGpioCallback(mButtonCallback)
        mButton?.setActiveType(Gpio.ACTIVE_LOW)
        mButton?.setDirection(Gpio.DIRECTION_IN)
        mButton?.setEdgeTriggerType(Gpio.EDGE_BOTH)

        mLed = service.openGpio(mLedPin)
        mLed?.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW)
    }

    override fun onDestroy() {
        super.onDestroy()
        mButton?.close()
        mLed?.close()
    }
}
