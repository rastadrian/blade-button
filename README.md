# blade-button ⚔️ 🔥
This is a small project to play with Android Things.

## Project Requirements
* Pico i.MX7D with AndroidThings 0.4-devpreview
* Rainbow 🌈 HAT for AndroidThings
* A display for the i.MX7D

## How to Install 💻
1. Connect your device to your computer.
2. Build and install the project in your device.

`./gradlew clean installDebug`

3. Run the apk. 🏃

`adb shell am start -a android.intent.action.MAIN -n com.rastadrian.androidthings/.HomeActivity`

__NOTE:__ You can also just open this project on Android Studio 3.0 and run it from there.

## How to Use 💁

Just tap the A button on your Rainbow HAT and embrace the fiery goodness.
